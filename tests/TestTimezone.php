<?php

namespace BonchDev\LaravelTimezone\Tests;

use BonchDev\LaravelTimezone\Timezone;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use BonchDev\LaravelTimezone\TimezoneFacade;
use Carbon\CarbonTimeZone;

class TestTimezone extends TestCase
{
    public function testTimezoneRegionName()
    {
        $tz = (new Timezone('Europe/Moscow'));

        $this->assertEquals(
            $tz->getTimezone(),
            Config::get('app.timezone')
        );

        $this->assertEquals(
            $tz->getTimezone(),
            Carbon::now()->getTimezone()
        );
    }

    public function testTimezoneOffsetName()
    {
        $tz = (new Timezone('+03:00'));

        $this->assertEquals(
            $tz->getTimezone(),
            Config::get('app.timezone')
        );

        $this->assertEquals(
            $tz->getTimezone(),
            Carbon::now()->getTimezone()
        );
    }
}