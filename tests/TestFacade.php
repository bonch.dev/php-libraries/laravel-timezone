<?php

namespace BonchDev\LaravelTimezone\Tests;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use BonchDev\LaravelTimezone\TimezoneFacade;

class TestFacade extends TestCase
{
    public function testFacade()
    {
        $timezone = TimezoneFacade::setTimezone('Europe/Moscow')
            ->getTimezone();

        $this->assertEquals(
            $timezone,
            Config::get('app.timezone')
        );

        $this->assertEquals(
            $timezone,
            Carbon::now()->getTimezone()
        );
    }

    public function testFacadeGetTimezone()
    {
        $timezone = TimezoneFacade::setTimezone('Europe/Moscow')
            ->getTimezone();

        $this->assertEquals(
            $timezone,
            Config::get('app.timezone')
        );

        $this->assertEquals(
            $timezone,
            Carbon::now()->getTimezone()
        );
    }
}