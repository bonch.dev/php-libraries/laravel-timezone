<?php

namespace BonchDev\LaravelTimezone\Tests;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use BonchDev\LaravelTimezone\TimezoneMiddleware;

class TestMiddleware extends TestCase
{
    public function testSetTimezoneConfigAndCarbon()
    {
        $test = $this;
        $request = new Request();
        $request->headers->set('X-Timezone', 'Europe/Belgrade');

        dump('Default timezone is ' . Config::get('app.timezone'));

        (new TimezoneMiddleware())->handle($request, function (Request $request) use ($test) {
            dump('Now timezone is ' . Config::get('app.timezone'));
            $test->assertEquals(
                $request->header('X-Timezone'),
                Config::get('app.timezone')
            );

            dump('Now time is ' . Carbon::now()->toAtomString());
            $test->assertEquals(
                Carbon::now()->getTimezone(),
                $request->header('X-Timezone')
            );
        } );
    }
}