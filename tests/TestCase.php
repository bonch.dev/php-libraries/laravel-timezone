<?php

namespace BonchDev\LaravelTimezone\Tests;

use BonchDev\LaravelTimezone\TimezoneServiceProvider;
use Orchestra\Testbench\TestCase as TestbenchTestCase;

class TestCase extends TestbenchTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            TimezoneServiceProvider::class
        ];
    }
}