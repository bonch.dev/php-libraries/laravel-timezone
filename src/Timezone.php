<?php 

namespace BonchDev\LaravelTimezone;

use Carbon\CarbonTimeZone;
use Exception;
use Illuminate\Support\Facades\Config;

class Timezone
{
    public $timezone = null;

    /**
     * Timezone Class constructor
     *
     * @param string $timezone
     */
    public function __construct($timezone = 'UTC')
    {
        $this->setTimezone($timezone);
    }

    /**
     * Check is timezone is offset
     *
     * @return boolean
     */
    private function isOffset(): bool
    {
        $timezone = $this->timezone;

        if (preg_match('/(\+|\-)[\d]{2}\:[\d]{2}/', $timezone)) {
            return true;
        }

        return false;
    }

    /**
     * Setting timezone
     *
     * @param [type] $timezone
     * @return self
     */
    public function setTimezone($timezone): self
    {
        $this->timezone = $timezone;

        if ($this->isOffset()) {
            $timezone = (new CarbonTimeZone($timezone))->toRegionName();
        }

        date_default_timezone_set($timezone);
        Config::set('app.timezone', $timezone);

        return $this;
    }

    public function getTimezone(bool $isRegion = true): string
    {
        $timezone = $this->timezone ?? Config::get('app.timezone', 'UTC');
        
        if ($isRegion) {
            $timezone = (new CarbonTimeZone($timezone))->toRegionName();
        } else {
            $timezone = (new CarbonTimeZone($timezone))->toOffsetName();
        }

        return $timezone;
    }
}