<?php

namespace BonchDev\LaravelTimezone;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class TimezoneServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('timezone', function () {
            return new Timezone(
                Config::get('app.timezone', 'UTC')
            );
        });
    }
}