<?php

namespace BonchDev\LaravelTimezone;

use BonchDev\LaravelTimezone\Timezone;
use Illuminate\Support\Facades\Facade;

/**
 * TimezoneFacade Class
 * 
 * @method static Timezone setTimezone(string $timezone) Set timezone (can be passed offset or region name)
 * @method static string getTimezone(bool $isRegion) Get timezone (can be return offset or region name)
 */
class TimezoneFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'timezone';
    }
}