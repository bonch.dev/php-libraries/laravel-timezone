<?php

namespace BonchDev\LaravelTimezone;

use Closure;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class TimezoneMiddleware
{
    /**
     * Accept X-Timezone header and set timezone in config 
     * and date_default_timezone_set() for Carbon
     *
     * @param $request
     * @param Closure $next
     * @return void
     */
    public function handle($request, Closure $next)
    {
        $timezone = $request->header('X-Timezone');
        
        if ($timezone) {
            TimezoneFacade::setTimezone($timezone);
        }
        
        return $next($request);
    }
}