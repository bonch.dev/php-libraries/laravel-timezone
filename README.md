# Bondh.dev Laravel Timezone

Small package that allow change default timezone in runtime. Timezone accepted in PHP DateTimeIdentifier (e.g `Europe/Moscow`) or hour offset, used in `DateTime::ATOM`.

## Features

- Timezone Class: Change timezone in `app.timezone` configuration and `date_default_timezone_set()` for Carbon.
Can be accepted both `+03:00` or `Europe/Moscow` formats.

- TimezoneFacade Class: fast access to TimezoneClass that already configured.
- TimezoneMiddleware Class: ready class for accept timezone from `X-Timezone` header from request.

## Requirements
- PHP 7.3 and above;
- Laravel 7 and above;

Honestly, package doesn't tested on other versions, so good luck!

## Installation

1. Run `composer require bonch.dev/laravel-timezone`.

## Using

- Timezone Class directly where it needed.
- Timezone Facade directly where it needed.
- Timezone Middleware for all routes or needed only.